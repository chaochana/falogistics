@extends('layouts.master')

@section('css')
    <!--<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/proximity_style.css') }}" />-->		
@stop

@section('header')

@stop

@section('content')

	<div class="row">
		<div class="large-4 medium-6 large-centered medium-centered small-centered columns">
			<center><i style='font-size:250px;' class="fi-map"></i></center>	
			
			<?php $messages = $errors->all('<p class="font-smaller" style="color:red">:message</p>') ?>
			<?php
				foreach ($messages as $msg)
				{
					echo $msg;
				}
			?>	
			<br/>
			<?= Form::open() ?>
			<?= Form::text('username', Input::old('username'), array('placeholder' => 'username')) ?>
			<?= Form::password('password', array('placeholder' => 'password')) ?>
			<?= Form::submit('Login', array('class' => 'small expand button')) ?>
			<?= Form::close() ?>
			
			<br/>
		</div>
	</div>

@stop

@section('js')
		<script type="text/javascript">
			$('#myModal').foundation('reveal', 'open');
			$('#myModal-history').foundation('reveal', 'open');
		</script>
@stop
