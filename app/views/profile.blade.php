@extends('layouts.master')

@section('css')
    <!--<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/proximity_style.css') }}" />-->		
@stop

@section('header')

@stop

@section('content')

    <div class='row'>
		<div class='large-6 medium-8 small-10 large-centered medium-centered small-centered columns'>							
			<ul class='large-block-grid-5 medium-block-grid-5 small-block-grid-2' style='text-align:center;'>
				<li  style='color:#2ba6cb'><i class='fi-ticket font-xlarge'></i><br/><b>order</b></li>
			</ul>
			<center><hr width=80%></center>
		</div>
	</div>
    <div class='row'>
		<div class='large-12 medium-12 small-12 large-centered medium-centered small-centered columns'>	
			<center><i class='fi-page-add font-large'></i> เพิ่ม Order ใหม่</center><br/>
			<table width=100% style='text-align:center'>
				<tr>
					<th>สถานที่</th>
					<th>ชื่อบริษัทปลายทาง</th>
					<th>ที่อยู่ปลายทาง</th>
					<th>เบอร์ติดต่อผู้รับ</th>
					<th>Order No.</th>
					<th>จำนวนกล่อง</th>			
					<th>รายละเอียดการส่ง</th>		
					<th>สถานะ</th>
					<th>ประวัติสถานะ</th>
					<th>ปรับปรุงข้อมูล</th>
				</tr>
				<?php
					//		$workpieces = Workpiece::with('member')->where('member_id','=',Auth::user()->id)->orderBy('created_at', 'DESC')->get();

					$orders = Member::find(Auth::user()->id)->orders;

					//$orders = Order::with('member')->

					//dd($orders);
				?>
				@foreach ($orders as $order)

					<tr>
						<td>{{ $order->destination }}</td>
						<td>{{ $order->destination_company }}</td>
						<td>{{ $order->destination_address }}</td>
						<td>{{ $order->contact_phone_number }}</td>
						<td>{{ $order->order_reference }}</td>
						<td>{{ $order->number_of_parcels }}</td>			
						<td><a href="#" data-reveal-id="myModal" data-reveal><i class='fi-info font-large'></i></a></td>
						<td><div style='color:#5da423'>ถึงผู้รับเรียบร้อย</div></td>	
						<td><a href="#" data-reveal-id="myModal-history" data-reveal><i class='fi-results-demographics font-large'></i></a></td>
						<td><i class='fi-page-edit font-large'></i></td>
					</tr>

				@endforeach
																											
			</table>
			<div class="pagination-centered">
			  <ul class="pagination">
			    <li class="arrow unavailable"><a href="">&laquo;</a></li>
			    <li class="current"><a href="">1</a></li>
			    <li><a href="">2</a></li>
			    <li><a href="">3</a></li>
			    <li><a href="">4</a></li>
			    <li class="unavailable"><a href="">&hellip;</a></li>
			    <li><a href="">12</a></li>
			    <li><a href="">13</a></li>
			    <li class="arrow"><a href="">&raquo;</a></li>
			  </ul>
			</div>
			
		</div>
	</div>
@stop

@section('js')
		<script type="text/javascript">
			$('#myModal').foundation('reveal', 'open');
			$('#myModal-history').foundation('reveal', 'open');
		</script>
@stop
