<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>FALogistics</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta http-equiv="Content-Language" content="th"> 
		
		<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js'></script>
		
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/foundation.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/foundation-icons.css') }}" />		
    	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/normalize.css') }}" />
   		<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/docs.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}"/>		

		@yield('css')

		<script src="js/modernizr.js"></script>		
			 		     
    </head>
	<body id="page">
		<div id="wrap">
			<div id="main">    		
				
				<div class="fixed">
					<nav class="top-bar">
						<ul class="title-area">
							<li class="name">
								<h1>
								 <a href="index.php">FA Logistics</a>
								</h1>
							</li>
							<li class="toggle-topbar menu-icon">
								<a href="#">
									<span>Menu</span>
								</a>
							</li>
						</ul>
						<section class="top-bar-section">
											
							<ul class="right">		
								<?php						
								if(!(Auth::check())){
								?>
								
								<li class="has-dropdown"><a href="#">login</a>
									<ul class="dropdown" style="width:300px;">
										<li class="has-form">
											<div class="row">
												<div class="column" style="color:white">

													<?= Form::open() ?>
													<?= Form::text('username', Input::old('username'), array('placeholder' => 'username')) ?>
													<?= Form::password('password', array('placeholder' => 'password')) ?>
													<?= Form::submit('Login', array('class' => 'small expand button')) ?>
													<?= Form::close() ?>
											
													<br/>
												</div>
											</div>
										</li>	
									</ul>
								</li>
								<?php
		  						} else {
		  						?>
								<li class="has-dropdown"><a href="#">{{ Auth::user()->username }}</a>
									<ul class="dropdown">
										<li class="has-form">
											<center><u><a href="{{ URL::asset('/logout') }}" style='color:red'>sign out</a></u></center>
										</li>	
									</ul>
								</li>
		  						<?php
		  						}
		  						?>									
							</ul>
						</section>
					</nav>
				</div>
				
				<div class="row">		
					
					@yield('content')
					
				</div><!-- End of Main Row -->
			</div><!-- End of class Main -->
		</div><!-- End of class wrapper -->		

		<div id="footer">
			<div class="row">
					<center>
					<h4 class='font-light-200'>FA-Logistics</h4>
					</center>
			</div>
		</div>		
		

		<script src="js/foundation/foundation.js"></script>
		<script src="js/foundation/foundation.alerts.js"></script>
		<script src="js/foundation/foundation.clearing.js"></script>
		<script src="js/foundation/foundation.cookie.js"></script>
		<script src="js/foundation/foundation.dropdown.js"></script>
		<script src="js/foundation/foundation.forms.js"></script>
		<script src="js/foundation/foundation.joyride.js"></script>
		<script src="js/foundation/foundation.magellan.js"></script>
		<script src="js/foundation/foundation.orbit.js"></script>
		<script src="js/foundation/foundation.reveal.js"></script>
		<script src="js/foundation/foundation.section.js"></script>
		<script src="js/foundation/foundation.tooltips.js"></script>
		<script src="js/foundation/foundation.topbar.js"></script>
		<script src="js/foundation/foundation.interchange.js"></script>
		<script src="js/foundation/foundation.placeholder.js"></script>

		@yield('js')

		<script>
		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		  document.write('<script src="http://foundation.zurb.com/docs/assets/vendor/'
			+ ('__proto__' in {} ? 'zepto' : 'jquery')
			+ '.js"><\/script>');
	
		</script>

		<script src="http://foundation3.zurb.com/docs/assets/vendor/custom.modernizr.js"></script>

		<script src="http://foundation3.zurb.com/docs/assets/docs.js"></script>

		<script>
			$(document).foundation();

			// For Kitchen Sink Page
			$('#start-jr').on('click', function() {
			$(document).foundation('joyride','start');
			});
		</script>	
	</body>
</html>

