<?php

	class PagesController extends BaseController {
		
		public function index(){
			return Redirect::to('login');
		}

		public function login(){
			return View::make('login');
		}

		public function member_profile($username){
			
			if (Auth::check())
			{
				return View::make('profile')->with('member',Auth::user());
			}
			else
			{
				return Redirect::to('login')->withErrors('You must login first.');
			}
		}

		public function admin_profile($username){
			
			if (Auth::check())
			{
				return View::make('admin_profile')->with('member',Auth::user());
			}
			else
			{
				return Redirect::to('login')->withErrors('You must login first.');
			}

		}		

/*
		public function login_post(){
			$rules = array(
				'username' => 'required',
				'password' => 'required'
				);
			
			$validation = Validator::make(Input::all(), $rules);
			
			if ($validation->fails())
			{
				return Redirect::to('/login')->withErrors($validation)->withInput();
			}

			$member = array(
				'username' => Input::get('username'),
				'password' => Input::get('password')
				);
			
			if (Auth::attempt($member))
			{
				if (Auth::user()->type == 1) {
					return Redirect::to('admin/'.Auth::user()->username);
				} else {
					return Redirect::to('member/'.Auth::user()->username);
				}	
			}

			return Redirect::to('/login')->withErrors('Could not log in.');
		}
*/
		
	}
