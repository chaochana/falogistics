<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function($table)
		    {
		        $table->increments('id')->unique();
		        $table->integer('members_id'); 
		        $table->string('destination');
		        $table->string('destination_company');
		        $table->string('destination_address');
				$table->string('contact_phone_number');
				$table->string('order_reference_number');						        
		        $table->integer('number_of_parcels');
		        $table->datetime('created_at');
		        $table->datetime('updated_at');
		    });//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
