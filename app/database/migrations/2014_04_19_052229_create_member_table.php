<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('members', function($table)
		    {
		        $table->increments('id')->unique(); 
		        $table->string('username')->unique();
		        $table->string('name')->nullable();
		        $table->string('lastname')->nullable();
		        $table->string('password',64);
		        $table->string('type');
				$table->string('mobile')->nullable();
		        $table->datetime('created_at');
		        $table->datetime('updated_at');
		    });//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('members');
	}

}
