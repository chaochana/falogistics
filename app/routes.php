<?php

Route::get('/', 'PagesController@index');

Route::get('/login', 'PagesController@login');

Route::post('/login', function(){
	$rules = array(
		'username' => 'required',
		'password' => 'required'
		);
	
	$validation = Validator::make(Input::all(), $rules);
	
	if ($validation->fails())
	{
		return Redirect::to('/login')->withErrors($validation)->withInput();
	}

	$member = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
		);
	
	if (Auth::attempt($member))
	{
		if (Auth::user()->type == 1) {
			return Redirect::to('admin/'.Auth::user()->username);
		} else {
			return Redirect::to('member/'.Auth::user()->username);
		}	
	}

	return Redirect::to('/login')->withErrors('Could not log in.');
});

Route::get('/member/{username}', 'PagesController@member_profile');

Route::get('/admin/{username}', 'PagesController@admin_profile');

Route::get('/logout', function()
{
	if (Auth::check())
	{	
		Auth::logout();
	}

	return Redirect::to('/');
});

/*
Event::listen('illuminate.query', function() {
    print_r(func_get_args());
});
*/